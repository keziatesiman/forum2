from django import forms

class Forum_Comment(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    comment_attrs = {
        'type': 'text',
        'cols': 100,
        'rows': 4,
        'class': 'todo-form-textarea form-control',
        'placeholder':'Masukan tanggapan anda..'

    }

    comment = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=comment_attrs))
