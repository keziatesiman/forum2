from django.db import models
from app_forum.models import Forum

class Comment(models.Model):
    forum = models.ForeignKey(Forum, null=True)
    comment = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
