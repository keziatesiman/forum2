from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .forms import Forum_Comment
from .models import Comment
from app_forum.models import Forum
# Create your views here.
def add_comment(request):
    form = Forum_Comment(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        comment = Comment(comment=request.POST['comment'], forum = Forum.objects.get(id=request.POST['id_forum']))
        print(comment.comment)
        print(comment.id)
        comment.save()
        return HttpResponseRedirect(reverse('app_login:index'))
    else:
        return HttpResponseRedirect(reverse('app_login:index'))
