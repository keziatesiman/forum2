from django.db import models
from app_login.models import Company

class Forum(models.Model):
    perusahaan = models.ForeignKey(Company, null=True)
    lowongan = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
