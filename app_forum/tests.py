from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
import requests
from app_login import *
import environ
from django.urls import reverse

# Create your tests here.
root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')
# Create your tests here.
class AppForumUnitTest(TestCase):

	def test_app_forum_url_is_exist(self):
		response = Client().get('/app_forum/')
		self.assertEqual(response.status_code, 302)

	def test_app_forum_using_index_func(self):
		found = resolve('/app_forum/')
		self.assertEqual(found.func, index)

