from django.conf.urls import url
from .views import index, add_forum

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_forum', add_forum, name='add_forum'),
]
