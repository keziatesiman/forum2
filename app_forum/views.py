from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Forum_Lowongan
from .models import Forum
from app_login.models import Company
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.urls import reverse


# Create your views here.
response = {}
def index(request): # pragma: no cover
    if "id_perusahaan" in request.session:
        response['logged_in'] = "true"
        response['nama'] = request.session['name']
    else:
        return HttpResponseRedirect(reverse('app_login:index'))
    response['author'] = "Kezia Irene"

    forum = Forum.objects.all().order_by('-created_date')

    #pagination
    paginator = Paginator(forum, 3)
    page = request.GET.get('page')
    try:
        halaman = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        halaman = paginator.page(1)

    response['Forum'] = halaman
    html = 'app_forum/app_forum.html'
    response['Forum_Lowongan'] = Forum_Lowongan
    return render(request, html, response)

def add_forum(request): # pragma: no cover
    form = Forum_Lowongan(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['lowongan'] = request.POST['lowongan']
        forum = Forum(lowongan=response['lowongan'], perusahaan = Company.objects.get(id_perusahaan=request.session['id_perusahaan']))
        forum.save()
        return HttpResponseRedirect('/app_forum/')
    else:
        return HttpResponseRedirect('/app_forum/')
