# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Company(models.Model):
    id_perusahaan = models.CharField('Id Perusahaan', max_length=20, primary_key=True, )
    nama = models.CharField('Nama', max_length=200)
    url_photo = models.CharField('Url Photo', max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
