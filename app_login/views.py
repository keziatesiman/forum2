from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .models import Company
from app_forum.models import Forum
from app_comments.models import Comment
from app_comments.forms import Forum_Comment
import requests

data = {}
response = {}
# Create your views here.
def index(request):
    if "id_perusahaan" in request.session:
        response['logged_in'] = "true"
        response['nama'] = request.session['name']
        response['url_photo'] = request.session['logoUrl']
    else:
        response['logged_in'] = None

    response['Forum'] = Forum.objects.all().order_by('-created_date')
    response['Comment'] = Comment.objects.all().order_by('-created_date')
    response['Forum_Comment'] = Forum_Comment
    return render(request, 'app_login.html', response)

# def auth(request):
#     url = "https://www.linkedin.com/oauth/v2/authorization"
#     params = {
#         "response_type" : "code",
#         "client_id" : "81fruveyl5xurr",
#         "redirect_uri" : "https://localhost:8000/login/callback",
#         "state" : "DCEeFWf45A53sdfKef424"
#     }

#     print(requests.get(url, params=params).text)
#     return render(request, 'login/login.html')

def callback(request):
    print("masuk sini")
    if request.method == "GET":
        code = request.GET.get('code', '')

        url = "https://www.linkedin.com/oauth/v2/accessToken"

        params = {
            "grant_type" : "authorization_code",
            "code" : str(code),
            "redirect_uri" : "http://localhost:8000/app_login/callback",
            "client_id" : "810u4iktbobjf4",
            "client_secret" : "8vd4NUXZhy9Scq76"
        }

        accessToken = requests.get(url, params=params).json()['access_token']

        params = {
            "oauth2_access_token" : accessToken,
            "format" : "json",
            "is-company-admin" : "true"
        }
        url2 = "https://api.linkedin.com/v1/companies"

        companyId = requests.get(url2, params=params).json()['values'][0]['id']

        params = {
            "oauth2_access_token" : accessToken,
            "format" : "json"
        }
        url3 = "https://api.linkedin.com/v1/companies/"+str(companyId)+":(id,name,company-type,website-url,logo-url,specialties)"

        jr = requests.get(url3, params=params).json()

        id_perusahaan = jr['id']
        tipePerusahaan = jr['companyType']['name']
        logoUrl = jr["logoUrl"]
        name = jr["name"]
        specialties = jr["specialties"]["values"]
        print(type(specialties))
        websiteUrl = jr["websiteUrl"]

        # request.session["id_perusahaan"] = id_perusahaan
        # request.session["name"] = name
        # request.session["logoUrl"] = logoUrl
        # request.session["tipePerusahaan"] = tipePerusahaan
        # request.session["specialties"] = specialties
        # request.session["websiteUrl"] = websiteUrl

        data = {}

        data["id_perusahaan"] = id_perusahaan
        data["name"] = name
        data["logoUrl"] = logoUrl
        data["tipePerusahaan"] = tipePerusahaan
        data["specialties"] = specialties
        data["websiteUrl"] = websiteUrl

        set_company(request, data)

        return HttpResponseRedirect(reverse('app_profile:showProfile'))
    else:
        return HttpResponseRedirect(reverse('app_login:index'))


def set_company(request, data):
    try:
        company = Company.objects.get(id_perusahaan=data['id_perusahaan'])
    except Exception as e:
        company = Company()
        company.id_perusahaan = data['id_perusahaan']
        company.nama = data['name']
        company.url_photo = data['logoUrl']
        company.save()

    request.session["id_perusahaan"] = data['id_perusahaan']
    request.session["name"] = data['name']
    request.session["logoUrl"] = data['logoUrl']
    request.session["tipePerusahaan"] = data['tipePerusahaan']
    request.session["specialties"] = data['specialties']
    request.session["websiteUrl"] = data['websiteUrl']

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('app_login:index'))
