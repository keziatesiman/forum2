from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

# Create your views here.
response = {}

def show_profile(request):
    if "id_perusahaan" in request.session:
        response['logged_in'] = "true"
        response['nama'] = request.session['name']
    else:
        return HttpResponseRedirect(reverse('app_login:index'))
    return render(request, 'compProfile.html', response)
